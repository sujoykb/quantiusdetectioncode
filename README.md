### Materials and Methods

**This repository is no longer updated. Please refer to this link for latest version: https://github.com/CCCofficial/quantiusdetectioncode**

~~This repository contains the representative implementation of the Fully Convolutional Regression Network used for the detection task in [1]. Please note the following directory structure.~~ 

~~`|`- code   
`|`- annotation  
`|`- data  
`|`- dependency [this folder is currently absent, please create it and add MatConvNet library here]~~  

~~The _annotation/9_gt_expert.csv_ has __one__ expert's annotation for the entire dataset (all 48 frames). The training session uses this ground truth. The other CSV files in the _annotation_ directory correspond to the annotations obtained from __six__ experts in total (used primarily for testing/evaluation puprose). The MATLAB script _code/demo_train_and_test.m_ demonstrates the full process.~~ 

~~** Dependency **~~

~~[MatConvNet: CNNs for MATLAB] (http://www.vlfeat.org/matconvnet/)~~


###### Citation

1. Quantius: Generic, high-fidelity human annotation of scientific images at 105-clicks-per-hour, Alex Hughes, Joseph D. Mornin, Sujoy K. Biswas, David P. Bauer, Simone Bianco, Zev J. Gartner

bioRxiv 164087; doi: https://doi.org/10.1101/164087 